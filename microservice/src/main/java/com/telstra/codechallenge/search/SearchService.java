package com.telstra.codechallenge.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jcabi.github.Github;
import com.jcabi.github.RtGithub;
import com.jcabi.http.response.JsonResponse;
import com.telstra.codechallenge.utils.Constants;

@Service
public class SearchService {
	
	@Value("${search.query.date}")
	private String DATE;

	@Value("${search.query.sort}")
	private String SORT;

	@Value("${search.query.path}")
	private String PATH;

	@Value("${search.query.parameter}")
	private String PARAMETER;

	public List<GitHubRepo> getHottestReposForAWeek(Integer count) throws IOException {
		final Github github = new RtGithub();

		final JsonResponse resp = github.entry().uri().path(PATH)
				.queryParam(PARAMETER, DATE + new DateTime().minusDays(7) + Constants.SPACE + SORT).back().fetch()
				.as(JsonResponse.class);
		final List<JsonObject> items = resp.json().readObject().getJsonArray(Constants.ITEMS)
				.getValuesAs(JsonObject.class);
		List<GitHubRepo> repoList = new ArrayList<>();
		items.stream().limit(count).forEach(item -> {
			repoList.add(GitHubRepo.builder().name(item.get(Constants.NAME).toString())
			.description(item.get(Constants.DESCRIPTION).toString())
			.language(item.get(Constants.DESCRIPTION).toString())
			.watchers_count(item.get(Constants.WATCHERS_COUNT).toString())
			.html_url(item.get(Constants.HTML_URL).toString()).build());
		});
		return repoList;
	}
	
}
