package com.telstra.codechallenge.search;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search")
public class SearchController {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	private SearchService searchService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getHottestReposForWeek/{count}")
	public ResponseEntity<Object> getHottestReposForWeek(@PathVariable Integer count) throws IOException {
		if (count != null) 
		{
			LOGGER.debug("Searching hottest repos for the week : ", count);			
			return new ResponseEntity(searchService.getHottestReposForAWeek(count), HttpStatus.OK);
		}
		return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
	}
}
