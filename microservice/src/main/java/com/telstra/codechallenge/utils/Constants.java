package com.telstra.codechallenge.utils;

public class Constants {
	public static final String SPACE = " ";
	public static final String ITEMS = "items";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String LANGUAGE = "language";
	public static final String WATCHERS_COUNT = "watchers_count";
	public static final String HTML_URL = "html_url";
}
