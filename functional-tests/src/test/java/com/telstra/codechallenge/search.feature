# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve top repositories for the week, based on stars.

  Scenario: Get top repositories based on stars
    Given url microserviceUrl
    And path '/search/getHottestReposForWeek/1'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
    [
	    {
	        name : '#string',
	        description : '#string',
	        language : '#string',
	        watchers_count : '#string',
	        html_url : '#string'
	    }
    ]
    """
